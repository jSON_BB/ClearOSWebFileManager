<?php

$lang['clearos_web_file_manager_app_name'] = 'ClearOS Web File Manager';
$lang['clearos_web_file_manager_app_description'] = 'ClearOS Web File Manager - A native web interface to manage all your files.';
