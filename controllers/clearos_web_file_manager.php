<?php

/**
 * ClearOS Web File Manager controller.
 *
 * @category   Apps
 * @package    ClearOS_Web_File_Manager
 * @subpackage Views
 * @author     Jason Bucotte <jasonb@visceralrealitygames.com>
 * @copyright  2021 Jason Bucotte / Visceral Reality Games, Inc.
 * @license    GPLv2
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearOS Web File Manager controller.
 *
 * @category   Apps
 * @package    ClearOS_Web_File_Manager
 * @subpackage Controllers
 * @author     Jason Bucotte <jasonb@visceralrealitygames.com>
 * @copyright  2021 Jason Bucotte / Visceral Reality Games, Inc.
 * @license    GPLv2
 */

class Clearos_web_file_manager extends ClearOS_Controller
{
    /**
     * ClearOS Web File Manager default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('clearos_web_file_manager');

        // Load views
        //-----------

        $this->page->view_form('clearos_web_file_manager', NULL, lang('clearos_web_file_manager_app_name'));
    }
}
